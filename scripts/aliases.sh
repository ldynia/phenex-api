#!/bin/ash

alias ..="cd .."
alias ll="ls -alF"
alias pyc="find $(pwd) -type f -name *.pyc -delete"
alias pyo="find $(pwd) -type f -name *.pyo -delete"
alias pycache="find $(pwd) -type d -name __pycache__ -delete"
