#!/bin/ash

# Set timezone
echo "Set timezone"
rm -f /etc/localtime
echo $TZ > /etc/timezone

# Clean python
find /src -type f -name *.pyo -delete
find /src -type f -name *.pyc -delete
find /src -type d -name __pycache__ -delete

if [ "$DEPLOYMENT" = "development" ]; then
  pip3 install --upgrade pip
  pip3 install -r requirements.txt
  pip3 install -e vendors/tus/flask-tus
  pip3 install -e vendors/tus/

  mkdir -p /src/storage/log

  # Link files to proper locations
  ln -s /src/scripts/aliases.sh /etc/profile.d/aliases.sh
  ln -s /src/config/services/supervisor.${DEPLOYMENT}.ini /etc/supervisor.d/supervisor.ini

  touch /run/app.pid \
    /run/app.sock \
    /src/storage/log/error.log \
    /src/storage/log/access.log

  chmod 644 /run/*

  # Start Supervisor Daemon
  /usr/bin/supervisord -j /run/supervisord.pid

  # infinity sleep
  sh -c 'while sleep 3600; do :; done'
fi
