import click
import uuid
from app import app_test
from app.constants import TESTING_EMAIL
from app.utilities.mail import Mail
from flask.cli import with_appcontext


@app_test.command("send-mail", help="Sending email to TESTING_EMAIL envar.")
@with_appcontext
def send_test_mail():
    context = {
        'username': 'ludd',
        'verification_code': '123abc',
        'verification_url': 'http://something.com'
    }

    config = {
        'subject': 'This is test email',
        'recipients': [TESTING_EMAIL],
        'body': 'Arriving mens good!'
    }

    Mail.send(**config, **context)
