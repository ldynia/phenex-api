from wtforms import ValidationError


class ExistsValidator(object):

    def __init__(self, model, message=None):
        self.model = model
        self.message = message

    def __call__(self, form, field):
        data = {field.name: field.data}
        object = self.model.objects.filter(**data).first()
        if not object:
            raise ValidationError(u'Provided %s does not exist.' % (field.name))
