from app import csrf
from app import redis
from app import JWT_ACCESS_EXPIRES
from app import JWT_REFRESH_EXPIRES
from app.components.frontend_user.frontend_user_schema import FrontendUserSchema
from app.components.frontend_user.frontend_user_form import FrontendUserLoginForm
from app.components.frontend_user.frontend_user_schema import FrontendUserSchema
from app.components.frontend_user.frontend_user_repository import FrontendUserRepository
from flask import request
from flask import jsonify
from flask import current_app
from flask_classful import route
from flask_classful import FlaskView
from flask_jwt_extended import jwt_required
from flask_jwt_extended import jwt_refresh_token_required
from flask_jwt_extended import get_jti
from flask_jwt_extended import get_raw_jwt
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import unset_jwt_cookies
from flask_jwt_extended import create_access_token
from flask_jwt_extended import create_refresh_token
from flask_jwt_extended import set_access_cookies
from flask_jwt_extended import set_refresh_cookies
from datetime import datetime


class AuthView(FlaskView):

    route_base = '/auth'
    trailing_slash = False
    repo = FrontendUserRepository()
    schema = FrontendUserSchema()

    @csrf.exempt
    @route('login', methods=['POST'])
    def login(self):
        form = FrontendUserLoginForm()
        if form.validate() is False:
            return jsonify({"errors": form.errors}), 400

        user = self.repo.find_one(email=form.data['email'])
        try:
            user.active = True
            user.metadata.ip = request.headers.get_all('X-Real-IP')[0]
        except Exception:
            user.metadata.ip = None

        user.last_login_at = datetime.utcnow()
        self.repo.save(user)

        user_data = self.schema.dump(user)

        # TODO Enable refresh_token if you decided to support it
        access_token = create_access_token(identity=user_data)
        # refresh_token = create_refresh_token(identity=user_data)
        access_jti = get_jti(encoded_token=access_token)
        # refresh_jti = get_jti(encoded_token=refresh_token)

        # write encoded tokens to cache with expiration date
        redis.set(access_jti, 'false', JWT_ACCESS_EXPIRES)
        # redis.set(refresh_jti, 'false', JWT_REFRESH_EXPIRES)

        response = jsonify({
            'access_token': access_token,
            # 'refresh_token': refresh_token
        })

        # set cookies
        response.set_cookie('acctkn', access_token, domain=current_app.config['JWT_COOKIE_DOMAIN'], max_age=current_app.config['JWT_ACCESS_TOKEN_EXPIRES'])
        # response.set_cookie('reftkn', refresh_token, domain=current_app.config['JWT_COOKIE_DOMAIN'], max_age=current_app.config['JWT_REFRESH_TOKEN_EXPIRES'])

        return response, 201

    @csrf.exempt
    @jwt_required
    @route('logout', methods=['DELETE'])
    def logout(self):
        user = get_jwt_identity()
        user = self.repo.find_one(email=user['email'])
        user.update(active=False)

        # Blacklist token and delete access token
        access_token = get_raw_jwt()['jti']
        redis.set(access_token, 'true')
        redis.delete(access_token)

        # remove cookies
        response = jsonify('')

        response.delete_cookie('acctkn', domain=current_app.config['JWT_COOKIE_DOMAIN'])
        # response.delete_cookie('reftkn', domain=current_app.config['JWT_COOKIE_DOMAIN'])

        return response, 204

    @csrf.exempt
    @jwt_refresh_token_required
    @route('token_refresh', methods=['POST'])
    def refresh(self):
        user = get_jwt_identity()
        new_access_token = create_access_token(identity=user)

        # write encoded tokens to cache with expiration date
        access_jti = get_jti(encoded_token=new_access_token)
        redis.set(access_jti, 'false', JWT_ACCESS_EXPIRES)

        # TODO remove tokens form response
        # set access cookie in response
        response = jsonify({'access_token': new_access_token})
        set_access_cookies(response, new_access_token)

        return response, 201
