from app.utilities.repository import Repository
from app.components.frontend_user.frontend_user_model import FrontendUser


class FrontendUserRepository(Repository):

    def __init__(self):
        # Invoking base constructor
        Repository.__init__(self, FrontendUser)
