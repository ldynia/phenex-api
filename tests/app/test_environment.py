import os
import pytest
from app.constants import HOST
from app.constants import APP_DEBUG
from app.constants import APP_SECRET
from app.constants import APP_TIMEZONE
from app.constants import JWT_SECRET
from app.constants import DEPLOYMENT
from app.constants import MAIL_SERVER
from app.constants import MAIL_PORT
from app.constants import MAIL_SENDER
from app.constants import MAIL_USERNAME
from app.constants import MAIL_PASSWORD
from app.constants import TESTING_EMAIL
from app.constants import TESTING_PASSWORD
from app.constants import TESTING_MONGO_DB_NAME
from app.constants import MONGO_DB_HOST
from app.constants import MONGO_DB_PORT
from app.constants import MONGO_DB_NAME
from app.constants import REDIS_HOST
from app.constants import REDIS_PORT
from app.constants import RABBITMQ_HOST
from app.constants import RABBITMQ_PORT
from app.constants import RABBITMQ_USER
from app.constants import RABBITMQ_PASSWORD
from app.constants import MIN_CODE_COVERAGE


def test_envars(app):
    assert_deployment()
    assert_debug(DEPLOYMENT)
    assert_testing(DEPLOYMENT)
    assert_secret()
    assert_host()
    assert_mail(DEPLOYMENT)
    assert_mongo()
    assert_redis()
    assert_rabbitmq()
    assert_timezone()
    assert_code_coverage(app)
    assert_files(app, DEPLOYMENT)


def assert_files(app, deployment):
    if deployment.startswith(('dev')):
        assert os.path.exists(app.config['APP_ROOT'] + '/.env.secrets')


def assert_secret():
    # 2x256-bit WEP Keys from https://randomkeygen.com/
    assert APP_SECRET is not None
    assert len(APP_SECRET) == 58

    assert JWT_SECRET is not None
    assert len(JWT_SECRET) == 58


def assert_deployment():
    assert DEPLOYMENT is not None
    assert DEPLOYMENT.startswith(('prod', 'dev', 'test'))


def assert_host():
    assert HOST is not None
    assert '.' in HOST


def assert_debug(deployment):
    if deployment.startswith(('dev', 'test')):
        assert APP_DEBUG is True
    else:
        assert APP_DEBUG is False


def assert_testing(deployment):
    assert TESTING_EMAIL is not None
    assert TESTING_PASSWORD is not None
    assert TESTING_MONGO_DB_NAME is not None

    if deployment.startswith(('dev', 'test')):
        assert TESTING_MONGO_DB_NAME.startswith(MONGO_DB_NAME)
        assert TESTING_MONGO_DB_NAME.endswith('test')


def assert_mail(deployment):
    assert MAIL_SERVER is not None
    assert MAIL_PORT is not None
    assert MAIL_PORT in (25, 465, 587)
    assert MAIL_SENDER is not None
    assert '@' in MAIL_SENDER
    assert len(MAIL_SENDER.split('.')) >= 2

    if deployment.startswith(('dev', 'test')):
        assert MAIL_SERVER == 'smtp.mailgun.org'
        assert MAIL_USERNAME is not None
        assert MAIL_PASSWORD is not None
        assert TESTING_EMAIL is not None
        assert '@' in TESTING_EMAIL
        assert len(TESTING_EMAIL.split('.')) >= 2
        assert TESTING_PASSWORD is not None
    else:
        assert MAIL_USERNAME is None
        assert MAIL_PASSWORD is None
        assert 'dtu.dk' in MAIL_SERVER


def assert_mongo():
    assert MONGO_DB_HOST is not None
    assert MONGO_DB_PORT is not None
    assert MONGO_DB_PORT == 27017
    assert MONGO_DB_NAME is not None


def assert_redis():
    assert REDIS_HOST is not None
    assert REDIS_PORT == 6379


def assert_rabbitmq():
    assert RABBITMQ_HOST is not None
    assert RABBITMQ_PORT == 5672
    assert RABBITMQ_USER is not None
    assert RABBITMQ_PASSWORD is not None


def assert_code_coverage(min_code_coverage):
    assert MIN_CODE_COVERAGE is not None
    assert isinstance(MIN_CODE_COVERAGE, int)


def assert_timezone():
    assert APP_TIMEZONE is not None
    assert APP_TIMEZONE.startswith('Europe')
    assert APP_TIMEZONE.endswith('Copenhagen')
