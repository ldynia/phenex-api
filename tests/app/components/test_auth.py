import pytest


def test_login_endpoint(client, auth, repo):
    user = repo['frontend_user'].delete_all()

    response = auth.register()
    assert response.status_code == 201

    response = auth.login(email='email@not.exist', password='wrong_pwd')

    assert response.status_code == 400
    assert 'not registered' in response.get_json()['errors']['email'][0]

    response = auth.verify()
    assert response.status_code == 201

    response = auth.login()
    assert response.status_code == 201

    # Calling log out to remove access_token from DB
    access_token = response.headers.get('Set-Cookie').split('; ')[0].split('=')[1]
    response = auth.logout(access_token)
    assert response.status_code == 204


def test_refresh_endpoint(auth, repo):
    user = repo['frontend_user'].delete_all()

    response = auth.register()
    assert response.status_code == 201

    response = auth.verify()
    assert response.status_code == 201

    response = auth.login()

    cookie_header = response.headers.get('Set-Cookie')
    access_token = cookie_header.split('; ')[0].split('=')[1]

    assert access_token != ""
    assert response.status_code == 201

    # TODO: Uncoment if you decided to add refresh token
    # refresh_token = cookie_header.split('; ')[0].split('=')[2]
    # response = auth.refresh_token(refresh_token)
    #
    # assert refresh_token != ""
    # assert response.status_code == 201

    access_token = response.headers.get('Set-Cookie').split('; ')[0].split('=')[1]
    response = auth.logout(access_token)
    assert response.status_code == 204


def test_logout_endpoint(auth, repo):
    user = repo['frontend_user'].delete_all()

    response = auth.register()
    assert response.status_code == 201

    response = auth.verify()
    assert response.status_code == 201

    response = auth.login()
    assert response.status_code == 201

    cookie_header = response.headers.get('Set-Cookie')
    access_token = cookie_header.split('; ')[0].split('=')[1]

    response = auth.logout(access_token)
    assert response.status_code == 204
